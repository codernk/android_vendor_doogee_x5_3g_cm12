PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/doogee/x5_3g/bin,system/bin)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/doogee/x5_3g/lib,system/lib)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/doogee/x5_3g/etc,system/etc)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/doogee/x5_3g/vendor,system/vendor)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/doogee/x5_3g/xbin,system/xbin)

